﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GloNS
{
    // 플레이어 상태
    public enum PlayerState
    {
        Idle,    // 평소상태
        Attack,         // 공격상태
        Dodge,          // 회피상태
        Dead            // 사망상태
    }
    // 몬스터 상태
    public enum MonsterState
    {
        Idle,    // 평소
        Attack, // 공격
        Dead            // 사망
    }
    public enum Monster1AttackWay
    {
        Forward = 0,
        Back,
        Right,
        Left,
        ForwardRight,
        BackRight,
        ForwardLeft,
        BackLeft,
        enumLenth
    }
    public enum MonsterDataCollomn
    {
        ID,
        HP,
        Speed,
        AttackDamage
    }
    public enum MonsterPatternState
    {
        Normal,
        Seek,
        Deadly,
        Dead
    }
    public enum SceneNumToName
    {
        Start,
        Main,
        Win,
        Lose
    }
    public enum ButtonControls
    {
        Left,
        Right
    }

    public struct SceneNames
    {
        public string StartScene;
        public string BattleScene;
        public string ResultScene;
        public void init()
        {
            StartScene = "Start";
            BattleScene = "BattleScene";
            ResultScene = "Result";
        }
    }
}

