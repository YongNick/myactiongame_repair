﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GloNS;

public class CharactorUI : GaugeUI
{
    //HP,MP UI
    [SerializeField]
    private Image m_playerMpUI;
    // 플레이어 어택 버튼
    [SerializeField]
    private Button m_playerAttackButton;
    // 플레이어 스크립트
    [SerializeField]
    private Player m_playerScr;

    // 플레이어에게서 가져와서 사용할 수치들

    private int m_mp;
    private int m_maxMp;

    public void SetPlayerScr(Player scr)
    {
        m_playerScr = scr;
    }
    // 초기화
    public override void init()
    {
        // 플레이어 스크립트를 받아옴
        // 받아온 스크립트에서 데이터를 가져옴
        SetHP(m_playerScr.GetCharaData().GetHP);
        SetMaxHP(m_playerScr.GetCharaData().GetMaxHP);

        m_mp = m_playerScr.GetCharaData().GetMP;
        m_maxMp = m_playerScr.GetCharaData().GetMaxMP;
    }

    // HP감소 함수
    public override void HPDown(int damage)
    {
        // HP 감소
        SetHP(GetHP - damage);
        m_playerScr.GetCharaData().SetHP(GetHP);
        // 감소된 수치에 따라 몇퍼센트인지 저장
        float HpPer = (float)GetHP /
            (float)GetMaxHP;
        // 저장한 퍼센트수치를 HP UI의 fillAmount로 설정
        GetHpUI.fillAmount = HpPer;
    }

    // 게이지 증가 함수
    public void GetGauge()
    {
        // 현재 MP가 최대MP보다 적을때만 
        if (m_playerScr.GetCharaData().GetMP < m_playerScr.GetCharaData().GetMaxMP)
        {
            // 현재 mp를 받아오고
            int nowMP = m_playerScr.GetCharaData().GetMP;
            // ++해줌
            ++nowMP;
            // 해준거 다시 set해줌
            m_playerScr.GetCharaData().SetMP(nowMP);
            // hp함수와 동일처리
            m_playerMpUI.fillAmount = (float)nowMP / (float)m_playerScr.GetCharaData().GetMaxMP;
        }
        // 현재 mp가 최대mp와 같을경우, 공격버튼 활성화
        if( m_playerScr.GetCharaData().GetMP >= m_playerScr.GetCharaData().GetMaxMP)
        {
            m_playerAttackButton.interactable = true;
        }
    }

    // 게이지 잃게하는 함수
    public void LossGague()
    {
        // 공격시 모든 게이지를 잃게할 것이므로, 플레이어 mp 0으로 만들어줌.
        m_playerScr.GetCharaData().SetMP(0);
        // UI fillamount 설정
        m_playerMpUI.fillAmount = (float)m_playerScr.GetCharaData().GetMP / (float)m_playerScr.GetCharaData().GetMaxMP;
        // 공격버튼 비활성화
        if (m_playerScr.GetCharaData().GetMP < m_playerScr.GetCharaData().GetMaxMP)
        {
            m_playerAttackButton.interactable = false;
        }
    }

    IEnumerator ExitCoroutine()
    {
        yield return new WaitForSeconds(1.5f);
        ManageScene.ChangeScene(SceneNumToName.Lose);
    }


}
