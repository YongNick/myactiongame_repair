﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GloNS;

public abstract class GaugeUI : MonoBehaviour
{
    //HP UI
    [SerializeField]
    private Image m_hpUI;

    // 스크립트에서 받아와서 사용할 수치변수
    private int m_hp;
    private int m_maxHp;

    public Image GetHpUI
    {
        get { return m_hpUI; }
    }
    public int GetHP
    {
        get { return m_hp; }
    }
    public int GetMaxHP
    {
        get { return m_maxHp; }
    }

    public void SetHpUI(Image img)
    {
        m_hpUI = img;
    }
    public void SetHP(int hp)
    {
        m_hp = hp;
    }
    public void SetMaxHP(int hp)
    {
        m_maxHp = hp;
    }

    public abstract void init();
    public abstract void HPDown(int damage);

}