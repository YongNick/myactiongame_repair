﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GloNS;
public class RightButton : ButtonControl
{
    public override void init(ControlManager control)
    {
        SetChractorControl(control);
    }
    public override void OnPointerDown(PointerEventData ped)
    {
        GetControlManager.RightButtonOn();
    }
    public override void OnPointerUp(PointerEventData ped)
    {
        GetControlManager.RightButtonUp();
    }
}

