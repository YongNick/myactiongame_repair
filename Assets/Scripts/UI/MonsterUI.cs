﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GloNS;

public class MonsterUI : GaugeUI
{

    // 몬스터 스크립트 변수
    private Monster m_monsterScr;

    public void SetMonsterScr(Monster scr)
    {
        m_monsterScr = scr;
    }



    // 몬스터 스크립트 받아서 초기화
    public override void init()
    {
        SetHP(m_monsterScr.GetMonsterData.GetHP);
        SetMaxHP(m_monsterScr.GetMonsterData.GetMaxHP);
    }

    // HP 감소 함수
    public override void HPDown(int damage)
    {
        SetHP(GetHP - damage);
        m_monsterScr.GetMonsterData.SetHP(GetHP);
        float HpPer = (float)GetHP /
            (float)GetMaxHP;
        // hp퍼센트가 0.4 이하일 경우, Deadly로 설정
        GetHpUI.fillAmount = HpPer;
    }
    IEnumerator ExitCoroutine()
    {
        yield return new WaitForSeconds(1.5f);
        ManageScene.ChangeScene(SceneNumToName.Win);
    }

}
