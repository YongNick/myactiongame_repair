﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GloNS;
public class StartButtons : MonoBehaviour {

    public void exitButton()
    {
        Application.Quit();
    }

    public void StartButton()
    {
        ManageScene.ChangeScene(SceneNumToName.Main);
    }

}
