﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GloNS;
public class LeftButton : ButtonControl
{
    public override void init(ControlManager control)
    {
        SetChractorControl(control);
    }
    public override void OnPointerDown(PointerEventData ped)
    {
        GetControlManager.LeftButtonOn();
    }
    public override void OnPointerUp(PointerEventData ped)
    {
        GetControlManager.LeftButtonUp();
    }
}

