﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GloNS;
public abstract class ButtonControl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    // 캐릭터 컨트롤에 대한 스크립트
    [SerializeField]
    private ControlManager m_controlScr;

    public ControlManager GetControlManager
    {
        get { return m_controlScr; }
    }

    public void SetChractorControl(ControlManager var)
    {
        m_controlScr = var;
    }

    public abstract void init(ControlManager control);
    public abstract void OnPointerDown(PointerEventData ped);
    public abstract void OnPointerUp(PointerEventData ped);

}
