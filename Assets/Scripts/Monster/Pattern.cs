﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GloNS;
public abstract class Pattern : MonoBehaviour
{
    // 몬스터 스크립트 가져와줄 변수
    private Monster m_monsterScr;

    // 몬스터 스크립트 세팅
    public void SetMonsterScr(Monster mon)
    {
        m_monsterScr = mon;
    }

    public Monster GetMonsterScr()
    {
        return m_monsterScr;
    }

    // 패턴에 따라 설정해 줄 함수들
    public abstract void init(Monster mon);
    public abstract void Run();

    public abstract void NormalPattern();
    public abstract void DeadlyPattern();

    public abstract void SpecialPattern();

    public abstract void StopCoroutine();
    public MonsterPatternState PatternStateCheck()
    {
        return m_monsterScr.GetMonsterData.GetPatternState;
    }

}
