﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour
{
    // 히트박스의 렌더러를 사용하기 위한 변수
    private MeshRenderer m_mesh;
    // 색상을 변경할것이기떄문에, 사용하는 변수
    private Color m_meshColor;
    private Color m_firstColor;
    private Collider m_col;
    // 폭발시 생성할 이펙트
    [SerializeField]
    private GameObject m_explosion;
    [SerializeField]
    private HitBoxParent m_parent;
    // Use this for initialization
    void Awake()
    {
        // 메시렌더러 가져와줌
        m_mesh = GetComponent<MeshRenderer>();
        m_col = GetComponent<Collider>();
        // 현재 색상으로 저장
        m_firstColor = m_mesh.material.color;
        m_meshColor = m_mesh.material.color;
        // 색 변화 코루틴 실행
       // StartCoroutine("ColorChange");
    }

    public void ShowBox()
    {
        gameObject.SetActive(true);
        m_col.enabled = false;
        m_mesh.material.color = m_firstColor;
        m_meshColor = m_firstColor;
        StartCoroutine("ColorChange");
    }

    // 색 변화(투명도 변화)코루틴
    IEnumerator ColorChange()
    {
        // 현재 색상의 a값을 가져와줌
        float a = m_meshColor.a;
        while (true)
        {
            // 0.05텀으로, 점점 밝아지게 만들어줌
            yield return new WaitForSeconds(0.05f);
            if (a < 0.7)
            {
                a += 0.02f;
                m_mesh.material.color = new Vector4(m_meshColor.r, m_meshColor.g, m_meshColor.b, a);
            }
            // 적정 이상 밝아졌을 경우, 
            else
            {
                // 위험을 표시해주기 위해, 히트박스가 깜빡거리게 해줄 코루틴 실행
                StartCoroutine("Warning");
                // 현재 a값을 저장
                m_meshColor.a = a;
                break;
            }
        }
        yield return 0;
    }
    // 깜빡이 코루틴
    IEnumerator Warning()
    {
        // 템프 변수
        int temp = 0;
        // 현재 색상 a값 저장
        float a = m_meshColor.a;
        // 깜빡거리게 하기 위해, 사용할 bool변수
        bool warningSwitch = true;
        // 깜빡거리게함
        while (true)
        {
            if (temp < 5)
            {
                if (warningSwitch)
                {
                    m_mesh.material.color = new Vector4(m_meshColor.r, m_meshColor.g, m_meshColor.b, m_meshColor.a);
                    warningSwitch = false;
                    temp++;
                }
                else
                {
                    m_mesh.material.color = new Vector4(m_meshColor.r, m_meshColor.g, m_meshColor.b, 0.3f);
                    warningSwitch = true;
                    temp++;
                }
            }
            else
            {
                // 깜빡거리는 횟수가 모두 찼을경우, 충돌할 수 있도록 콜라이더 enable을 true로 바꿔줌
                m_col.enabled = true;
                // 폭발이펙트 생성
                // 오브젝트 제거
                yield return new WaitForSeconds(0.1f);
                //Instantiate(m_explosion, transform.position, transform.rotation);
                m_explosion.transform.position = transform.position;
                m_explosion.SetActive(true);
                m_col.enabled = false;
                gameObject.SetActive(false);
                break;
            }
            yield return new WaitForSeconds(0.15f);
        }
    }

    void OnTriggerEnter(Collider col)
    {

        if(col.CompareTag("Player"))
        {
            m_parent.GetBattleManager.PlayerDamaged();
        }
    }
}
