﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GloNS;
public class Monster1Pattern : Pattern
{
    
    // 공격위치를 저장해둘 배열변수
    Monster1AttackWay[] m_monsterAttackWays;
    // Seek패턴으로 돌릴지 아닐지 설정할 변수
    private bool isSeek = false;

    // 몬스터 공격위치 초기화
    void MonsterAttackWaysInit()
    {
        m_monsterAttackWays = new Monster1AttackWay[(int)Monster1AttackWay.enumLenth];
        m_monsterAttackWays[(int)Monster1AttackWay.Forward] = Monster1AttackWay.Forward;
        m_monsterAttackWays[(int)Monster1AttackWay.Left] = Monster1AttackWay.Left;
        m_monsterAttackWays[(int)Monster1AttackWay.Right] = Monster1AttackWay.Right;
        m_monsterAttackWays[(int)Monster1AttackWay.Back] = Monster1AttackWay.Back;
        m_monsterAttackWays[(int)Monster1AttackWay.ForwardLeft] = Monster1AttackWay.ForwardLeft;
        m_monsterAttackWays[(int)Monster1AttackWay.ForwardRight] = Monster1AttackWay.ForwardRight;
        m_monsterAttackWays[(int)Monster1AttackWay.BackLeft] = Monster1AttackWay.BackLeft;
        m_monsterAttackWays[(int)Monster1AttackWay.BackRight] = Monster1AttackWay.BackRight;
    }

    // 초기화 함수, 현재 몬스터 스크립트를 받아와서 초기화함
    public override void init(Monster mon)
    {
        MonsterAttackWaysInit();
        // 몬스터 스크립트 저장,몬스터의 데이터를 사용해야할지도 모르기 때문
        SetMonsterScr(mon);
        Run();
    }
    // 노말패턴
    public override void NormalPattern()
    {
        // 플레이어 찾는중인지, 아닌지에 따라 공격,찾기 코루틴 실행
        if (!isSeek)
        {
            StartCoroutine(NormalAttack());
        }
        else if (isSeek)
        {
            StartCoroutine(SeekPlayer());
        }
    }

    public override void Run()
    {
        // 기본적으로 노말패턴 실행
        NormalPattern();
    }

    // 데들리패턴
    public override void DeadlyPattern()
    {
        if (!isSeek)
        {
            StartCoroutine(DeadlyAttack());
        }
        else if (isSeek)
        {
            StartCoroutine(SeekPlayer());
        }
    }

    // 특수공격
    public override void SpecialPattern()
    {
        StartCoroutine(SpecialAttack());
    }

    public override void StopCoroutine()
    {
        StopAllCoroutines();
    }

    // 기본공격 코루틴
    IEnumerator NormalAttack()
    {
        yield return new WaitForFixedUpdate();
        // 공격후 플레이어를 바라보게 하기 위해, true로 변경
        isSeek = true;
        // 2초의 텀을 주고 공격
        yield return new WaitForSeconds(2.0f);
        AttackForward();

        // 공격 후, 몬스터 상태를 받아와 체크해줌
        if (PatternStateCheck() == MonsterPatternState.Normal)
        {
            NormalPattern();
        }
        else if (PatternStateCheck() == MonsterPatternState.Deadly)
        {
            DeadlyPattern();
        }
    }

    // 데를리 상태일때 공격 코루틴
    IEnumerator DeadlyAttack()
    {
        yield return new WaitForFixedUpdate();
        isSeek = true;
        // 특수 공격은 1/4확률로 발동하게 설정함.
        int SpecialAttack = Random.Range(0, 4);

        if (SpecialAttack == 2)
        {
            SpecialPattern();
        }
        else
        {
            // 특수공격 패턴이 아닐경우, 순차적으로 공격함수 실행
            DeadlyAttack1();
            yield return new WaitForSeconds(1.5f);
            DeadlyAttack2();
            yield return new WaitForSeconds(1.5f);
            DeadlyAttack3();
            yield return new WaitForSeconds(3.5f);
            DeadlyPattern();
        }
    }
    // 플레이어 찾는 코루틴
    IEnumerator SeekPlayer()
    {
        yield return new WaitForFixedUpdate();
        // 몬스터 패턴상태 변경
        GetMonsterScr().GetMonsterData.SetPatternState(MonsterPatternState.Seek);
        yield return new WaitForSeconds(5.0f);
        // 5초간 찾게한후, 다시 원래 몬스터 패턴으로 돌려줌.
        float HPper = (float)GetMonsterScr().GetMonsterData.GetHP/
            (float)GetMonsterScr().GetMonsterData.GetMaxHP;
        if (HPper <= 0.4f)
        {
            GetMonsterScr().GetMonsterData.SetPatternState(MonsterPatternState.Deadly);
        }
        else
        {
            GetMonsterScr().GetMonsterData.SetPatternState(MonsterPatternState.Normal);
        }
        isSeek = false;
        // 돌아온 패턴에 따라 다시 패턴실행
        if (PatternStateCheck() == MonsterPatternState.Normal)
        {
            NormalPattern();
        }
        else if (PatternStateCheck() == MonsterPatternState.Deadly)
        {
            DeadlyPattern();
        }
        else
        {
            yield return null;
        }
    }
    void AttackForward()
    {
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Forward]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.ForwardLeft]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.ForwardRight]);
    }
    void DeadlyAttack1()
    {
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Forward]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.ForwardLeft]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.ForwardRight]);
    }
    void DeadlyAttack2()
    {
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Left]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Right]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.ForwardLeft]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.ForwardRight]);
    }
    void DeadlyAttack3()
    {
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Forward]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Left]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Right]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Back]);
    }

    IEnumerator SpecialAttack()
    {
        yield return new WaitForFixedUpdate();
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Forward]);
        yield return new WaitForSeconds(0.4f);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.ForwardLeft]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.ForwardRight]);
        yield return new WaitForSeconds(0.4f);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Right]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Left]);
        yield return new WaitForSeconds(0.4f);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.BackLeft]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.BackRight]);
        yield return new WaitForSeconds(0.4f);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Back]);
        yield return new WaitForSeconds(2.3f);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Forward]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.ForwardLeft]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.ForwardRight]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Right]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Left]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.BackLeft]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.BackRight]);
        GetMonsterScr().Attack(m_monsterAttackWays[(int)Monster1AttackWay.Back]);
        yield return new WaitForSeconds(3f);
        DeadlyPattern();
    }
}
