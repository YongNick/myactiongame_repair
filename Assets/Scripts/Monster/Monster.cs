﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GloNS;

public class Monster : MonsterBase
{
    [SerializeField]
    private GameObject[] m_hitObjPoses;


    // 초기화 함수 데이터를 받아와서 초기화
    public override void Init(MonsterData data,List<GameObject> hitboxes,List<HitBoxParent> hitboxscr,BattleManager battlemanager)
    {
        SetBattleManager(battlemanager);
        SetMonsterData(data);
        // UI 설정,초기화
        // 애니메이션 컴포넌트 가져옴
        SetAnimation(GetComponent<Animation>());
        // 몬스터 아이디에 따라, 패턴설정 현재는 1개
        switch (GetMonsterData.GetID)
        {
            case 1:
                SetMonsterPattern(GetComponent<Monster1Pattern>());
                GetMonsterPattern.init(GetComponent<Monster>());
                break;
        }
    }

    // 플레이어 바라볼수 있도록 해주는 함수.
    public override void Lookplayer()
    {
        // RotateToWards를 사용해 바라볼 수 있도록 설정했음
        Vector3 targetDir = GetPlayerObj.transform.position - transform.position;
        Quaternion targetRot = Quaternion.LookRotation(targetDir);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRot, GetMonsterData.GetRotateSpeed * Time.deltaTime);
    }

    // 데미지 입는 함수
    public override void GetDamage(int damage)
    {
        GetBattleManager.GetMonsterUI.HPDown(damage);
    }

    // 공격위치에 따라 위치에 히트박스 생성해주는 함수
    public void Attack(Monster1AttackWay way)
    {
        int num = (int)way;
        GameObject tempObj = GetBattleManager.HitBoxPop();
        HitBoxParent tempScr = GetBattleManager.HitBoxScrPop();
        tempObj.transform.position = GetBattleManager.GetMonster1AttackWay(way).position;
        tempObj.transform.rotation = GetBattleManager.GetMonster1AttackWay(way).rotation;
        tempScr.WakeUp();
    }
    public void Dead()
    {
        if (GetAnimation().isPlaying)
        {
            var tempAnim = GetAnimation();
            tempAnim.CrossFade("Anim_Death");
        }
        // 몬스터 패턴상태,상태 모두 dead로 돌려줌
        GetMonsterData.SetPatternState(MonsterPatternState.Dead);
        GetMonsterData.SetState(MonsterState.Dead);
    }



    public override void UIMove()
    {
        if (GetMonsterUI != null)
        {
            Vector3 temptemp =
                transform.position - new Vector3(0, -15.0f, 0);
            GetMonsterUI.transform.position = GetCamera.WorldToScreenPoint(temptemp);
        }
    }
    void Update()
    {
        if (GetPlayerObj != null)
        {
            if (GetMonsterData.GetPatternState == MonsterPatternState.Seek)
            {
                Lookplayer();
            }
        }
        UIMove();
    }

}
