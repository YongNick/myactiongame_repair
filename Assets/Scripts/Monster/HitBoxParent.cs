﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxParent : MonoBehaviour {

    [SerializeField]
    private Hitbox m_hitBoxScr;
    [SerializeField]
    private GameObject m_Explosion;
    private BattleManager m_battleManager;

    public void SetBattleManager(BattleManager manager)
    {
        m_battleManager = manager;
    }
    public BattleManager GetBattleManager
    {
        get { return m_battleManager; }
    }

    public void WakeUp()
    {
        gameObject.SetActive(true);
        m_Explosion.SetActive(false);
        m_hitBoxScr.ShowBox();
        StartCoroutine(ActiveFalse());
    }

    IEnumerator ActiveFalse()
    {
        yield return new WaitForSeconds(7.2f);
        m_battleManager.HitBoxScrPush(this);
        m_battleManager.HitBoxPush(gameObject);
    }
}
