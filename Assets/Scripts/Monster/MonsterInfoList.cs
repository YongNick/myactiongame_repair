﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 몬스터 리스트 받아오고, 스태틱으로 저장해 줄 클래스
public class MonsterInfoList
{
    // 받아온 몬스터 데이터 스태틱으로 저장해 둘 리스트
    private static List<MonsterData> m_list = null;

    // 리스트의 인스턴스를 사용하기 위한 스태틱.
    public static List<MonsterData> GetInstanse
    {
        get
        {
            if (m_list == null)
            {
                m_list = new List<MonsterData>();
            }
            return m_list;
        }
    }

    public int GetLength()
    {
        return m_list.Count;
    }

    public void AddToList(MonsterData data)
    {
        m_list.Add(data);
    }

    public void DeleteFromList(MonsterData data)
    {
        m_list.Remove(data);
    }


    // 몬스터를 id로 찾는 함수.
    public static MonsterData FindMonsterByID(int id)
    {
        var tempData = m_list.Find(val => val.GetID == id);
        if (tempData == null)
        {
            UnityEngine.Debug.LogError("Data Not Exist");
            return null;
        }
        return tempData;
    }


    /*
    private static int[] m_id;
    private static int[] m_hp;
    private static float[] m_fSpeed;
    private static int[] m_attackDamage;
    private static int m_length;

    public static int GetMonsterID(int val)
    {
        return m_id[val];
    }
    public static int GetMonsterHP(int val)
    {
        return m_hp[val];
    }
    public static float GetMonsterSpeed(int val)
    {
        return m_fSpeed[val];
    }
    public static int GetMonsterDamage(int val)
    {
        return m_attackDamage[val];
    }
    public static int GetLength()
    {
        return m_length;
    }
    public static int GetAddress(int val)
    {
        return val - 1;
    }


    public static void SetMonsterID(int address, int val)
    {
        m_id[address] = val;
    }
    public static void SetMonsterHP(int address, int val)
    {
        m_hp[address] = val;
    }
    public static void SetMonsterSpeed(int address, float val)
    {
        m_fSpeed[address] = val;
    }
    public static void SetMonsterDamage(int address, int val)
    {
        m_attackDamage[address] = val;
    }

    public static void Init(int val)
    {
        m_id = new int[val];
        m_hp = new int[val];
        m_fSpeed = new float[val];
        m_attackDamage = new int[val];
        m_length = val;

    }*/
}