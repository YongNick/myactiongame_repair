﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GloNS;

// 몬스터 데이터 저장용 클래스
public class MonsterData
{
    private MonsterState m_state;
    private MonsterPatternState m_PatternState;
    private int m_id;
    private int m_hp;
    private int m_maxHp;
    private float m_rotateSpeed;
    private int m_attackDamage;

    public MonsterState GetState
    {
        get
        {
            return m_state;
        }
    }
    public MonsterPatternState GetPatternState
    {
        get { return m_PatternState; }
    }
    public int GetID
    {
        get { return m_id; }
    }
    public int GetHP
    {
        get { return m_hp; }
    }
    public float GetRotateSpeed
    {
        get { return m_rotateSpeed; }
    }
    public int GetAttackDamage
    {
        get { return m_attackDamage; }
    }
    public int GetMaxHP
    {
        get { return m_maxHp; }
    }

    public void SetPatternState(MonsterPatternState state)
    {
        m_PatternState = state;
    }
    public void SetState(MonsterState state)
    {
        m_state = state;
    }
    public void SetID(int val)
    {
        m_id = val;
    }
    public void SetMaxHP(int val)
    {
        m_maxHp = val;
    }
    public void SetAttackDamage(int val)
    {
        m_attackDamage = val;
    }
    public void SetHP(int val)
    {
        m_hp = val;
    }
    public void SetRotateSpeed(float val)
    {
        m_rotateSpeed = val;
    }

    public MonsterData(int id,
                   int hp,
                   float speed,
                   int damage)
    {
        m_id = id;
        m_hp = hp;
        m_maxHp = hp;
        m_rotateSpeed = speed;
        m_attackDamage = damage;
        m_PatternState = MonsterPatternState.Normal;
        m_state = MonsterState.Idle;
    }
}
