﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GloNS;

public abstract class MonsterBase : MonoBehaviour
{
    // 몬스터데이터 저장용 변수
    private MonsterData m_monsterInfodata;
    // UI 제어용 변수
    private MonsterUI m_monsterUI;
    // 플레이어 받아올 변수
    private GameObject m_playerObj;
    private Camera m_mainCamera;
    private Pattern m_monsterPattern;
    private Animation m_anim;
    private BattleManager m_battleManager;


    public void SetBattleManager(BattleManager scr)
    {
        m_battleManager = scr;
    }
    public BattleManager GetBattleManager
    {
        get { return m_battleManager; }
    }
    public void SetMonsterPattern(Pattern pattern)
    {
        m_monsterPattern = pattern;
    }
    public Pattern GetMonsterPattern
    {
        get { return m_monsterPattern; }
    }
    public void SetCamera(Camera cam)
    {
        m_mainCamera = cam;
    }

    public void SetUI(MonsterUI ui)
    {
        m_monsterUI = ui;
    }

    public Animation GetAnimation()
    {
        return m_anim;
    }
    public MonsterUI GetMonsterUI
    {
        get { return m_monsterUI; }
    }
    public MonsterData GetMonsterData
    {
        get { return m_monsterInfodata; }
    }
    public GameObject GetPlayerObj
    {
        get { return m_playerObj; }
    }
    public Camera GetCamera
    {
        get { return m_mainCamera; }
    }
    
    public void SetAnimation(Animation ani)
    {
        m_anim = ani;
    }
    public void SetPlayerObj(GameObject obj)
    {
        m_playerObj = obj;
    }
    // 초기화 함수 데이터를 받아와서 초기화
    public abstract void Init(MonsterData data, List<GameObject> hitboxes, List<HitBoxParent> hitboxscr, BattleManager battlemanager);

    // 플레이어 바라볼수 있도록 해주는 함수.
    public abstract void Lookplayer();

    // 데미지 입는 함수
    public abstract void GetDamage(int damage);
    public abstract void UIMove();


    public void SetMonsterData(MonsterData data)
    {
        int id = data.GetID;
        int hp = data.GetHP;
        float speed = data.GetRotateSpeed;
        int damage = data.GetAttackDamage;
        m_monsterInfodata = new MonsterData(id, hp, speed, damage);
    }
}
