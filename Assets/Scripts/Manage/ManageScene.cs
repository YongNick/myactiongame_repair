﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GloNS;

public static class ManageScene
{
    public static void ChangeScene(SceneNumToName SceneName)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene((int)SceneName);
    }
}
