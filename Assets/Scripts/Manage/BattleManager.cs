﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using GloNS;
//<summary>
public class BattleManager : MonoBehaviour
{
    [SerializeField]
    private CharactorUI m_playerUI;
    [SerializeField]
    private MonsterUI m_monsterUI;
    [SerializeField]
    private Camera m_mainCamera;
    [SerializeField]
    private Transform m_playerRadiusCenter;
    [SerializeField]
    private GameObject[] m_monster1AttackWays;
    // 플레이어 총알 오브젝트. 씬에 생성되어있는게 이상한것이라면
    // 플레이어에서 한번 생성 후 active로 관리할 것
    [SerializeField]
    private GameObject m_playerBullet;
    [SerializeField]
    private ControlManager m_playerControlManager;
    [SerializeField]
    private GameObject m_bulletGoal;

    [SerializeField]
    private Transform m_playerSpawnpos;
    [SerializeField]
    private Transform m_monsterSpawnpos;

    private GameObject m_hitBox;
    [SerializeField]
    private List<GameObject> m_hitBoxes = null;
    [SerializeField]
    private List<HitBoxParent> m_hitBoxScrs = null;
    private int m_hitBoxPooledAmount = 20;


    private GameObject m_bullet;
    private GameObject m_bulletExplosion;
    private List<GameObject> m_bullets = null;
    private List<GameObject> m_bulletExplosions = null;
    private int m_bulletPoolAmount = 3;

    private GameObject[] m_monsterResources;
    private GameObject m_playeResource;
    private GameObject m_loseCanvas;
    private GameObject m_winCanvas;

    private GameObject m_monsterObj;
    private GameObject m_playerObj;

    private Monster m_monster;
    private Player m_player;
    private CharactorControl m_playerControl;
    private int m_monsterID;

    // 어느 한 쪽이 사망한지 확인하기 위한 변수
    private bool isdead = false;


    public MonsterUI GetMonsterUI
    {
        get { return m_monsterUI; } 
    }
    void Awake()
    {
        // 리소스 로드
        m_hitBoxes = new List<GameObject>();
        m_hitBoxScrs = new List<HitBoxParent>();
        m_bullets = new List<GameObject>();
        m_bulletExplosions = new List<GameObject>();
        m_bullet = Resources.Load("Bullet") as GameObject;
        m_bulletExplosion = Resources.Load("BulletExplosion") as GameObject;
        m_hitBox = Resources.Load("HitBoxSet") as GameObject;
        m_playeResource = Resources.Load("Player") as GameObject;
        m_monsterResources = new GameObject[2];
        m_monsterResources[0] = Resources.Load("Monster_1") as GameObject;
        m_monsterResources[1] = Resources.Load("Monster_2") as GameObject;
        m_loseCanvas = Resources.Load("LoseCanvas") as GameObject;
        m_winCanvas = Resources.Load("WinCanvas") as GameObject;
        init();
    }

    public void init()
    {
        CreateHitBoxPool();
        isdead = false;
        //StartCoroutine(Test());

        // 플레이어 생성/초기화부분
        m_playerObj = Instantiate(m_playeResource, m_playerSpawnpos.position, new Quaternion(0, 0, 0, 0));
        m_player = m_playerObj.GetComponent<Player>();
        m_playerControl = m_player.GetComponent<CharactorControl>();
        m_playerControl.SetBattleManager(this);
        PlayerBaseSetting();
        m_player.Init();
        m_playerControlManager.SetCharaControl(m_playerControl);
        m_mainCamera.gameObject.transform.SetParent(m_playerObj.transform);
        // 몬스터 랜덤생성/초기화부분
        m_monsterID = Random.Range(1, 3);
        // 현재 두번째 몬스터는 스크립트 추가가 되어있지 않으므로, 생성확인만 하고 고정생성으로 바꿔두었음
        m_monsterObj = Instantiate(m_monsterResources[0], m_monsterSpawnpos.position, new Quaternion(0, 0, 0, 0));
        for (int i = 0; i < m_monster1AttackWays.Length; i++)
        {
            m_monster1AttackWays[i].transform.parent = m_monsterObj.transform;
        }
        m_monster = m_monsterObj.GetComponent<Monster>();
        MonsterBaseSetting();
        m_monster.Init(MonsterInfoList.FindMonsterByID(1), m_hitBoxes, m_hitBoxScrs, GetComponent<BattleManager>());
        m_monsterUI.SetMonsterScr(m_monster);
        m_monsterUI.init();
        m_playerUI.SetPlayerScr(m_player);
        m_playerUI.init();
        CreateBulletPool();
        CreateBulletExplosionPool();
    }

    public Transform GetMonster1AttackWay(Monster1AttackWay attackway)
    {
        int num = (int)attackway;
        return m_monster1AttackWays[num].transform;
    }

    // 몬스터 기본세팅. 함수로 정리, 필요없다면 init에 바로 추가
    void MonsterBaseSetting()
    {
        m_monster.SetPlayerObj(m_playerObj);
        m_monster.SetCamera(m_mainCamera);
    }

    // 몬스터 기본세팅과 동일
    void PlayerBaseSetting()
    {
        m_player.SetUI(m_playerUI);
        m_player.SetCamera(m_mainCamera);
        m_player.SetBullet(m_playerBullet);
        m_player.SetCenter(m_playerRadiusCenter);
    }

    void CreateHitBoxPool()
    {
        for (int i = 0; i < m_hitBoxPooledAmount; i++)
        {
            GameObject tempObj = Instantiate(m_hitBox);
            HitBoxParent tempScr = tempObj.GetComponent<HitBoxParent>();
            tempScr.SetBattleManager(this);
            HitBoxScrPush(tempScr);
            HitBoxPush(tempObj);
        }
    }
    void CreateBulletPool()
    {
        for (int i = 0; i < m_bulletPoolAmount; i++)
        {
            GameObject tempObj = Instantiate(m_bullet);
            Bullet tempScr = tempObj.GetComponent<Bullet>();
            tempScr.SetBattleManager(this);
            tempScr.SetGoal(m_bulletGoal);
            BulletPush(tempObj);
        }
    }
    void CreateBulletExplosionPool()
    {
        for (int i = 0; i < m_bulletPoolAmount; i++)
        {
            GameObject tempObj = Instantiate(m_bulletExplosion);
            BulletExplosion tempScr = tempObj.GetComponent<BulletExplosion>();
            tempScr.SetBattleManager(this);
            BulletExplosionPush(tempObj);
        }
    }

    public GameObject BulletPop()
    {
        if (m_bullets.Count > 0)
        {
            GameObject tempObj = m_bullets[0];
            m_bullets.RemoveAt(0);
            return tempObj;
        }
        else
        {
            return Instantiate(m_bullet) as GameObject;
        }
    }
    public void BulletPush(GameObject obj)
    {
        m_bullets.Add(obj);
        obj.SetActive(false);
    }

    public GameObject BulletExplosionPop()
    {
        if (m_bulletExplosions.Count > 0)
        {
            GameObject tempObj = m_bulletExplosions[0];
            m_bulletExplosions.RemoveAt(0);
            return tempObj;
        }
        else
        {
            return Instantiate(m_bulletExplosion) as GameObject;
        }
    }
    public void BulletExplosionPush(GameObject obj)
    {
        m_bulletExplosions.Add(obj);
        obj.SetActive(false);
    }

    public GameObject HitBoxPop()
    {
        if (m_hitBoxes.Count > 0)
        {
            GameObject tempObj = m_hitBoxes[0];
            m_hitBoxes.RemoveAt(0);
            return tempObj;
        }
        else
        {
            return Instantiate(m_hitBox) as GameObject;
        }
    }
    public void HitBoxPush(GameObject obj)
    {
        m_hitBoxes.Add(obj);
        obj.transform.parent = transform;
        //obj.transform.position = Vector3.zero;
        obj.SetActive(false);
    }
    public HitBoxParent HitBoxScrPop()
    {
        if (m_hitBoxScrs.Count > 0)
        {
            HitBoxParent tempScr = m_hitBoxScrs[0];
            m_hitBoxScrs.RemoveAt(0);
            return tempScr;
        }
        else
        {
            return null;
        }
    }
    public void HitBoxScrPush(HitBoxParent scr)
    {
        m_hitBoxScrs.Add(scr);
    }


    void FixedUpdate()
    {
        // 누군가 죽지 않았을 때만 한 번 발동.
        if (!isdead)
        {
            if (m_player != null && m_monster != null)
            {
                // 플레이어,몬스터중 한 명이 HP가 0이 될 경우
                if (m_player.GetCharaData().GetHP <= 0 || m_monster.GetMonsterData.GetHP <= 0)
                {
                    // 루프를 방지하기 위해 true로 설정
                    isdead = true;
                    // 둘 다 행동을 못하게 설정
                    m_monster.GetMonsterPattern.StopCoroutine();
                    m_player.GetCharaData().SetState(PlayerState.Dead);
                    m_monster.GetMonsterData.SetState(MonsterState.Dead);
                    // 플레이어 사망시 그에따른 함수호출, 코루틴
                    if (m_player.GetCharaData().GetHP <= 0)
                    {
                        m_playerControl.Dead();
                        StartCoroutine(PlayerDead());
                    }
                    // 몬스터 사망시 그에따른 함수호출, 코루틴
                    else if (m_monster.GetMonsterData.GetHP <= 0)
                    {
                        m_monster.Dead();
                        StartCoroutine(MonsterDead());
                    }
                }
            }
        }
    }


    IEnumerator PlayerDead()
    {
        yield return new WaitForSeconds(1.5f);
        SceneNames scene = new SceneNames();
        scene.init();
        GameObject.Find("Manager").GetComponent<Manager>().CallResultScene(scene.ResultScene, false);
    }

    IEnumerator MonsterDead()
    {
        yield return new WaitForSeconds(1.5f);
        SceneNames scene = new SceneNames();
        scene.init();
        GameObject.Find("Manager").GetComponent<Manager>().CallResultScene(scene.ResultScene, true);
    }

    public void PlayerDamaged()
    {
        m_playerControl.Dameged();

    }
    public void MonsterDamaged(int dam)
    {
        m_monster.GetDamage(dam);

    }

}
//</summary>
