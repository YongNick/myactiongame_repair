﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultScene : MonoBehaviour {

    [SerializeField]
    private GameObject m_winImage;
    [SerializeField]
    private GameObject m_loseImage;
	// Use this for initialization
	void Awake () 
    {
        bool isWin = GameObject.Find("Manager").GetComponent<Manager>().GetWinLose;	
        if(isWin)
        {
            m_winImage.SetActive(true);
        }
        else
        {
            m_loseImage.SetActive(false);
        }
	}
}
