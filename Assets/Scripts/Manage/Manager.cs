﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using GloNS;

public class Manager : MonoBehaviour
{

    // 데이터를 받아와주기 위해 존재
    private TextAsset m_data;
    private bool m_winlose;

    public bool GetWinLose
    {
        get { return m_winlose; }
    }
    void Awake()
    {
        // 데이터 로드
        m_data = Resources.Load("monsterdata", typeof(TextAsset)) as TextAsset;
        DontDestroyOnLoad(gameObject);
        CreateList();
    }
    // 리스트 생성.
    private void CreateList()
    {
        StringReader sr = new StringReader(m_data.text);
        string source = sr.ReadLine();

        source = sr.ReadLine();
        string[] values;
        while (source != null)
        {
            values = source.Split(' ');
            if (values.Length == 0)
            {
                sr.Close();
                return;
            }
            var tempMobData = new MonsterData(
                int.Parse(values[(int)MonsterDataCollomn.ID]),
                int.Parse(values[(int)MonsterDataCollomn.HP]),
                float.Parse(values[(int)MonsterDataCollomn.Speed]),
                int.Parse(values[(int)MonsterDataCollomn.AttackDamage]));

            MonsterInfoList.GetInstanse.Add(tempMobData);
            #region
            /*
            for (int i = 0; i < values.Length; i++)
            {
                int tempi;
                float tempf;
                switch(i)
                {
                    case 0:
                        tempi = int.Parse(values[i]);
                        MonsterInfoList.SetMonsterID(nowListAddress, tempi);
                        break;
                    case 1:
                        tempi = int.Parse(values[i]);
                        MonsterInfoList.SetMonsterHP(nowListAddress, tempi);
                        break;
                    case 2:
                        tempf = float.Parse(values[i]);
                        MonsterInfoList.SetMonsterSpeed(nowListAddress, tempf);
                        break;
                    case 3:
                        tempi = int.Parse(values[i]);
                        MonsterInfoList.SetMonsterDamage(nowListAddress, tempi);
                        break;

                }
            }*/
            #endregion
            source = sr.ReadLine();
        }
        sr.Close();
    }

    public void CallResultScene(string name,bool isWin)
    {
        m_winlose = isWin;
        SceneNames scenename = new SceneNames();
        scenename.init();
        if (name == scenename.ResultScene)
        {
            SceneManager.LoadScene(name);
        }
        else
        {
            Debug.Log("That scenename doesn't exist ");
        }
    }
}
