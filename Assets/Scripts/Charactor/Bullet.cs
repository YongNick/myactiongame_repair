﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    // 인스펙터에서 설정가능한 속도
    public float Speed;


    // 목표위치
    private GameObject m_goal;
    // 목표에 도달해서 폭발하는 이펙트
    [SerializeField]
    private GameObject m_explosion;
    private BattleManager m_battleManager;

    public void SetBattleManager(BattleManager scr)
    {
        m_battleManager = scr;
    }

    public void SetGoal(GameObject goal)
    {
        m_goal = goal;
    }


    // 스피드 멤버
    private float m_speed;
    
    public float GetSpeed
    {
        get { return m_speed; }
    }

    public void SetSpeed(float val)
    {
        m_speed = val;
    }

	private void Update () 
    {
        move();
	}

    // 목표(몬스터)를 향해 이동하도록 설정.
    private void move()
    {
        transform.LookAt(m_goal.transform);
        transform.Translate(new Vector3(0, 0, Speed) * Time.deltaTime);
    }

    //몬스터와 충돌시, 폭발 오브젝트를 활성화, 총알(bullet오브젝트는 다시 원위치)
    void OnTriggerEnter(Collider col)
    {
        if(col.CompareTag("Monster"))
        {
            col.GetComponent<Monster>().GetDamage(1);
            GameObject tempObj = m_battleManager.BulletExplosionPop();
            tempObj.transform.position = transform.position;
            tempObj.SetActive(true);
            m_battleManager.BulletPush(gameObject);
        }
    }
}
