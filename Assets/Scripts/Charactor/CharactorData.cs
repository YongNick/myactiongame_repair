﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GloNS;

public class CharactorData
{
    // 플레이어 데이터
    private int m_hp;
    private int m_maxHp;
    private int m_mp;
    private float m_fspeed;
    private int m_maxMp;
    private float m_radius;
    private PlayerState m_playerState;

    // 생성자, 데이터 저장
    public CharactorData(int hp,
                   int mp,
                   int maxMp,
                   float speed,
                   float radius,
                   PlayerState state)
    {
        m_hp = hp;
        m_maxHp = hp;
        m_mp = mp;
        m_maxMp = maxMp;
        m_fspeed = speed;
        m_radius = radius;
        m_playerState = state;
    }

    public PlayerState GetState
    {
        get{return m_playerState;}
    }
    public float GetRadius
    {
        get { return m_radius; }
    }
    public int GetMaxMP
    {
        get { return m_maxMp; }
    }
    public int GetMP
    {
        get { return m_mp; }
    }
    public int GetMaxHP
    {
        get { return m_maxHp; }
    }
    public int GetHP
    {
        get { return m_hp; }
    }
    public float GetSpeed
    {
        get { return m_fspeed; }
    }

    public void SetState(PlayerState state)
    {
        m_playerState = state;
    }
    public void SetHP(int val)
    {
        m_hp = val;
    }
    public void SetMaxHP(int val)
    {
        m_maxHp = val;
    }
    public void SetMP(int val)
    {
        m_mp = val;
    }
    public void SetMaxMP(int val)
    {
        m_maxMp = val;
    }
    public void SetRadius(float val)
    {
        m_radius = val;
    }
}