﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletExplosion :MonoBehaviour
{
    private BattleManager m_battleManager;

    public void SetBattleManager(BattleManager manager)
    {
        m_battleManager = manager;
    }

    void OnDisable()
    {
        m_battleManager.BulletExplosionPush(gameObject);
    }

}
