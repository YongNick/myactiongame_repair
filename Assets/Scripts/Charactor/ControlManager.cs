﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GloNS;
public class ControlManager : MonoBehaviour
{
    // 플레이어 컨트롤 변수
    private CharactorControl m_playerControl;
    // 왼쪽오른쪽 버튼 컨트롤 변수
    [SerializeField]
    private ButtonControl m_LeftbuttonControl;
    [SerializeField]
    private ButtonControl m_RightbuttonControl;

    // 플레이어 컨트롤 설정
    public void SetCharaControl(CharactorControl con)
    {
        m_playerControl = con;
    }

    // 초기화
    public void init(CharactorControl control)
    {
        m_playerControl = control;
        m_LeftbuttonControl.init(this);
        m_RightbuttonControl.init(this);
    }

    // 버튼 작동에 따라 플레이어 컨트롤에서 함수 호출
    public void LeftButtonOn()
    {
        m_playerControl.LeftButtonOn();
    }
    public void LeftButtonUp()
    {
        m_playerControl.LeftButtonUp();
    }
    public void RightButtonOn()
    {
        m_playerControl.RightButtonOn();
    }
    public void RightButtonUp()
    {
        m_playerControl.RightButtonUp();
    }

    public void Dodge()
    {
        m_playerControl.Dodge();
    }
    public void Attack()
    {
        m_playerControl.Attack();
    }
}