﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GloNS;

public class Player : Charactor
{
    // 데이터를 다른데서 가져오지 않으므로, 인스펙터창에서 설정가능하도록
    // 변수 설정해줌
    public int HP;
    public int MP;
    public int MaxMP;
    public float Radius;
    public float Speed;
    
    // 원 그리며 이동할수 있도록, 중심축을 잡아주는 센터 transform
    [SerializeField]
    private Transform m_radiusCenter;
    // 플레이어 총알
    [SerializeField]
    private GameObject m_bullet;

    // 플레이어 UI스크립트 가져와주기 위한 변수
    // 플레이어컨트롤 스크립트 가져와주기위한 변수
    [SerializeField]
    private CharactorControl m_playerControl;
    public void SetBullet(GameObject bullet)
    {
        m_bullet = bullet;
    }
    public void SetCenter(Transform center)
    {
        m_radiusCenter = center;
    }
    public GameObject GetBullet 
    {
        get { return m_bullet; }
    }
    // 초기화
    public override void Init()
    {
        SetAnimator(GetComponent<Animator>());
        CharactorData tempData = new CharactorData(HP, MP, MaxMP, Speed, Radius, PlayerState.Idle);
        SetCharaData(tempData);
        m_playerControl.Init(GetComponent<Player>(), GetPlayerUI, GetCamera);
    }

    // Update is called once per frame
    void Update()
    {
        // 이동축을 계속 잡아줘야하므로, 업데이트에서 함수 계속 실행
        AngleSetting();
        // 플레이어 컨트롤의 Move 함수로 이동
        m_playerControl.Move();
    }

    // 축을 잡아주기 위한 함수
    private float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    private float GetDegree(Vector2 from, Vector2 to)
    {
        return Mathf.Atan2(from.y - to.y, to.x - from.x) * Mathf.Rad2Deg;
    }
    private void AngleSetting()
    {

        transform.eulerAngles = new Vector3(0, 90 + GetDegree(new Vector2(m_radiusCenter.position.x, m_radiusCenter.position.z), new Vector2(transform.position.x, transform.position.z)), 0);
        Vector3 tempt = new Vector3(transform.position.x - m_radiusCenter.position.x, 0, transform.position.z - m_radiusCenter.position.z);
        Vector3 tempt2 = tempt.normalized * GetCharaData().GetRadius;
        transform.position = Vector3.Lerp(transform.position, new Vector3(m_radiusCenter.position.x + tempt2.x, transform.position.y, m_radiusCenter.position.z + tempt2.z), Time.deltaTime * 2);
    }

    // 충돌판정 시
    void OnTriggerEnter(Collider col)
    {
        // 적 공격일 경우, 플레이어컨트롤 스크립트에서 판정을 위한 함수 호출
        if (col.CompareTag("EnemyAttack"))
        {
            m_playerControl.GetMonsterAttack();
        }
    }
}
