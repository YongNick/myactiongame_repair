﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GloNS;

public abstract class Charactor : MonoBehaviour
{
    // 플레이어 hp,mp,상태
    private CharactorData m_charaData;

    // 플레이어 모델
    [SerializeField]
    private GameObject m_model;

    // 애니메이터
    private Animator m_animator;
    [SerializeField]
    private CharactorUI m_playerUI;

    private Camera m_mainCamera;

    public void SetCamera(Camera cam)
    {
        m_mainCamera = cam;
    }
    public Camera GetCamera
    {
        get { return m_mainCamera; }
    }
    public void SetUI(CharactorUI ui)
    {
        m_playerUI = ui;
    }

    public CharactorUI GetPlayerUI
    {
        get { return this.m_playerUI; }
    }
    
    // 초기화작업
    public abstract void Init();

    public GameObject GetPlayerModel 
    {
        get { return m_model; }
    }
    // 변수들의 get전용 
    public Animator GetAnimator
    {
        get { return m_animator; }
    }
    public CharactorData GetCharaData()
    {
        return m_charaData;
    }
    public void SetAnimator(Animator val)
    {
        m_animator = val;
    }
    public void SetCharaData(CharactorData data)
    {
        m_charaData = data;
    }

    public void SetAnimatorStateRun(bool val)
    {
        m_animator.SetBool("Run", val);
    }
    public void SetAnimatorStateAttack(bool val)
    {
        m_animator.SetBool("Attack", val);
    }
    public void SetAnimatorStateDead(bool val)
    {
        m_animator.SetBool("Dead", val);
    }
    public void SetAnimatorStateDodge(bool val)
    {
        m_animator.SetBool("Dodge", val);
    }
}