﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GloNS;

public class CharactorControl : MonoBehaviour
{
    //플레이어 스크립트를 저장할 변수
    private Player m_playerScr;
    // 플레이어 트랜스폼 저장용 변수
    private Transform m_playerTransform;
    // UI컨트롤을 위한 변수
    private CharactorUI m_playerUI;
    // 버튼컨트롤을 위한 변수
    private BattleManager m_battleManager;

    // 총알이 다시 풀링될 지점
    [SerializeField]
    private GameObject m_bulletSpawnPos;
    [SerializeField]
    private Camera m_mainCamera;

    // 한번의 회피로 여러번의 게이지를 먹을 수 없도록 하기 위한 변수
    private bool isGetGauge = false;

    // 버튼 컨트롤용 변수
    private bool LeftButtonDown = false;
    private bool RightButtonDown = false;

    public void SetBattleManager(BattleManager scr)
    {
        m_battleManager = scr;
    }
    public void SetCamera(Camera cam)
    {
        m_mainCamera = cam;
    }

    // 버튼 컨트롤용 함수
    public void LeftButtonOn()
    {
        LeftButtonDown = true;
    }
    public void LeftButtonUp()
    {
        LeftButtonDown = false;
        m_playerScr.SetAnimatorStateRun(false);
    }
    public void RightButtonOn()
    {
        RightButtonDown = true;
    }
    public void RightButtonUp()
    {
        RightButtonDown = false;
        m_playerScr.SetAnimatorStateRun(false);
    }
     
    // 초기화작업
    public void Init(Player playerData,CharactorUI uiscr, Camera cam)
    {
        m_playerScr = playerData;
        m_playerTransform = GetComponent<Transform>();
        m_playerUI = uiscr;
        m_mainCamera = cam;
    }

    // 캐릭터 이동변수
    public void Move()
    {
        // 캐릭터의 상태가 Idle일때만, 이동,회피,공격 가능하도록
        if (m_playerScr.GetCharaData().GetState == PlayerState.Idle)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Dodge();
            }
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                Attack();
            }
            if (Input.GetKey(KeyCode.A) || LeftButtonDown)
            {
                RunLeft();
            }
            else if (Input.GetKey(KeyCode.D) || RightButtonDown)
            {
                RunRight();
            }
        }
        // 키보드 조작시 애니매이션 출력 가능하도록 한 것. 지금은 안드로이드로 할것이기 때문에 주석
        
        if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A))
        {
            m_playerScr.SetAnimatorStateRun(false);
        }
        
        // 버튼이 아무것도 눌리지 않았을경우 Idle애니매이션으로 돌아올 수 있도록 체크
        if (!LeftButtonDown && !RightButtonDown)
        {
        }
    }
    // 데미지 입는 함수
    public void Dameged()
    {
        // UI에서 HP게이지 따로 관리하기때문에 불러와서 써줌
        if (m_playerScr.GetCharaData().GetState != PlayerState.Dodge)
        {
            m_playerUI.HPDown(1);
        }
    }

    // Damaged함수와 마찬가지로,게이지는 UI에서 관리하기 때문에 불러와줌
    public void GetGauge()
    {
        m_playerUI.GetGauge();
    }

    // 회피함수
    public void Dodge()
    {
        // 회피처리를 위한 코루틴 실행
        if (m_playerScr.GetCharaData().GetState == PlayerState.Idle)
        {
            StartCoroutine(dodgeCorutine());
        }
    }

    // 왼쪽으로 달리는 함수.
    public void RunLeft()
    {
        // 플레이어를 데이터에서 받아온 속도로 이동
        m_playerTransform.transform.Translate(new Vector3(m_playerScr.GetCharaData().GetSpeed, 0, 0) * Time.deltaTime);
        // 모델이 왼쪽을 보도록 설정
        m_playerScr.GetPlayerModel.transform.localEulerAngles = new Vector3(0, 90, 0);
        // 애니메이션 활성화
        m_playerScr.SetAnimatorStateRun(true);

    }
    // 왼쪽함수과 동일
    public void RunRight()
    {
        m_playerTransform.transform.Translate(new Vector3(-m_playerScr.GetCharaData().GetSpeed, 0, 0) * Time.deltaTime);
        m_playerScr.GetPlayerModel.transform.localEulerAngles = new Vector3(0, 270, 0);
        m_playerScr.SetAnimatorStateRun(true);
    }
    public void Dead()
    {
        StopAllCoroutines();
        m_playerScr.GetCharaData().SetState(PlayerState.Dead);
        m_playerScr.SetAnimatorStateRun(false);
        m_playerScr.SetAnimatorStateAttack(false);
        m_playerScr.SetAnimatorStateDodge(false);
        m_playerScr.SetAnimatorStateDead(true);
    }
    // 공격함수
    public void Attack()
    {
        // 플레이어의 MP가 최대MP수치와 같을경우 공격가능하도록
        if (m_playerScr.GetCharaData().GetMP == m_playerScr.GetCharaData().GetMaxMP)
        {
            GameObject tempObj = m_battleManager.BulletPop();
            tempObj.transform.position = m_bulletSpawnPos.transform.position;
            tempObj.SetActive(true);
            // 생성되어있는 총알의 위치를 플레이어 위치로 가져오고
            /*
            m_playerScr.GetBullet.transform.position
                = m_bulletSpawnPos.transform.position;
            // 총알을 액티브시켜준다
            m_playerScr.GetBullet.SetActive(true);*/
            // 공격했으므로, 게이지를 잃는 함수 실행
            m_playerUI.LossGague();
        }
    }

    // 회피판정 코루틴
    IEnumerator dodgeCorutine()
    {
        // 회피애니메이션 활성화
        m_playerScr.SetAnimatorStateDodge(true);
        // 플레이어의 상태를 회피중으로 변경
        m_playerScr.GetCharaData().SetState(PlayerState.Dodge);
        // 회피시 이동 코루틴 실행
        StartCoroutine("dodgeMove");

        // 닷지애니메이션 비활성화, 애니메이션 버그가 나지 않도록 0.6초의 텀을 둠
        // 회피는 총 1.2초의 판정을 가짐
        yield return new WaitForSeconds(0.6f);
        m_playerScr.SetAnimatorStateDodge(false);
        yield return new WaitForSeconds(0.6f);
        // 회피판정 후 다시 idle상태로 돌려줌
        m_playerScr.GetCharaData().SetState(PlayerState.Idle);
    }
    IEnumerator dodgeMove()
    {
        // 회피시 왼쪽으로 이동할것인지 오른쪽으로 이동할것인지 설정을 위한 변수
        bool leftright;
        // 모델의 로컬오일러앵글외 Y각이 90도 이하일 경우
        if (GetModelLocalEulerAngleY <= 90)
        {
            // 왼쪽으로 이동할 수 있도록 false
            leftright = false;
        }
        // 모델의 로컬오일러앵글외 Y각이 270도 이상일 경우
        else if (GetModelLocalEulerAngleY >= 270)
        {
            // 오른쪽으로 이동하도록 true설정
            leftright = true;
        }
        else
        {
            // 이동하지 않았을경우 기본값으로 오른쪽으로 이동하도록 설정
            leftright = true;
        }
        // 회피하는동안(판정이 있는동안)이동하도록 while로 묶어줌
        while (true)
        {
            if (m_playerScr.GetCharaData().GetState == PlayerState.Dodge)
            {
                yield return 0;
                if (leftright)
                {
                    RunRight();
                }
                else if (!leftright)
                {
                    RunLeft();
                }
            }
            else
            {
                break;
            }
        }
        m_playerScr.SetAnimatorStateRun(false);
        yield return 0;
    }
    // 회피판정시 게이지 얻는 코루틴
    IEnumerator Dodgejudge()
    {
        yield return new WaitForFixedUpdate();
        if (isGetGauge)
        {
            isGetGauge = false;
            GetGauge();
            yield return new WaitForSeconds(1.2f);
        }
    }
    // 사망시 씬을 넘기기위한 코루틴
    IEnumerator ExitCoroutine()
    {
        yield return new WaitForSeconds(3.0f);
        ManageScene.ChangeScene(SceneNumToName.Lose);
    }

    // 공격받았을시, 회피판정을 위한 함수
    public void GetMonsterAttack()
    {
        // 캐릯터의 상태를 받아옴
        switch (m_playerScr.GetCharaData().GetState)
        {
                // 회피상태일 경우
            case PlayerState.Dodge:
                // 게이지를 얻는것을 true로
                isGetGauge = true;
                if (isGetGauge)
                {
                    //회피판정 활성화
                    StartCoroutine("Dodgejudge");
                }
                break;
                // 일반상태일경우
            case PlayerState.Idle:
                // 데미지입는함수 
                Dameged();
                break;
        }
    }

    // 모델의 로컬오일러앵글Y값 가져오는 변수
    private float GetModelLocalEulerAngleY
    {
        get { return m_playerScr.GetPlayerModel.transform.localEulerAngles.y; }
    }
    private void UIMove()
    {
        if (m_playerUI != null)
        {
            Vector3 temptemp =
                m_playerScr.GetPlayerModel.transform.position - new Vector3(0, 0.5f, 0);
            m_playerUI.transform.position = m_mainCamera.WorldToScreenPoint(temptemp);
        }
    }
    void Update()
    {
        UIMove();
    }
}
